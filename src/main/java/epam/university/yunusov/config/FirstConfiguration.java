package epam.university.yunusov.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("epam.university.yunusov.bean.beans1")
public class FirstConfiguration {

}

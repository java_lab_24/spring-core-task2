package epam.university.yunusov.config;

import epam.university.yunusov.bean.beans3.BeanD;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "epam.university.yunusov.bean",
    includeFilters = @Filter(type = FilterType.REGEX, pattern = "[(.*Flower.*)(.*BeanD.*)(.*BeanF.*)]"))
public class SecondConfiguration {

  public BeanD getBeanD() {
    return new BeanD();
  }
}

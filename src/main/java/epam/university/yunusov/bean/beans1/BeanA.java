package epam.university.yunusov.bean.beans1;

import epam.university.yunusov.bean.other.OtherBeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanA {

  @Autowired
  private OtherBeanA otherBeanA;
}

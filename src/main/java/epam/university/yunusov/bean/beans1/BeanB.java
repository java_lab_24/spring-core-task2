package epam.university.yunusov.bean.beans1;

import epam.university.yunusov.bean.other.OtherBeanB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanB {

  private OtherBeanB otherBeanB;

  @Autowired
  public BeanB(OtherBeanB otherBeanB) {
    this.otherBeanB = otherBeanB;
  }
}

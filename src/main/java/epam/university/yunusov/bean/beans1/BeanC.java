package epam.university.yunusov.bean.beans1;

import epam.university.yunusov.bean.other.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanC {

  private OtherBeanC otherBeanC;

  @Autowired
  public void setOtherBeanC(OtherBeanC otherBeanC) {
    this.otherBeanC = otherBeanC;
  }
}

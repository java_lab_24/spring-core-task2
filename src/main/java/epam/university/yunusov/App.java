package epam.university.yunusov;

import epam.university.yunusov.config.FirstConfiguration;
import epam.university.yunusov.config.SecondConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

  public static void main(String[] args) {
    ApplicationContext context = new AnnotationConfigApplicationContext(FirstConfiguration.class);
    ApplicationContext context2 = new AnnotationConfigApplicationContext(SecondConfiguration.class);
  }

}
